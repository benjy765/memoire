% !TEX root = memoire.tex

\chapter{Chaînes de Markov}\label{markov}

\begin{defi}
Soit $\mathcal{P}$ un processus stochastique définit par $X_0, X_1, X_2, ...$ une suite de variable aléatoire chacune à valeur dans l'ensemble $S = \{0, 1, ..., M \}$. Alors, $\mathcal{P}$ est une chaîne de Markov si $q(X_{n+1} = j | X_n = i, X_{n-1} = i_{n-1}, ..., X_0 = i_0) = q(X_{n+1} = j|X_n = i).$ 
\end{defi}

L'ensemble $S$ peut être interprété comme l'état d'un système au cours du temps de sorte que $X_n = i$ indique que l'état du système au temps $n$ est $i$. La probabilité d'une transition au temps $n+1$ sachant les états au temps inférieur est donc une quantité d'intérêt. La caractéristique d'une chaîne de Markov est que cette probabilité de transition dépend seulement de l'état au temps $n$. La probabilité conjointe d'une chaîne de Markov se factorise de la manière suivante:
\begin{equation}
q(x_n, x_{n-1}, ..., x_0) = q(x_0)q(x_1|x_0)q(x_2|x_1) ... q(x_n|x_{n-1}).
\end{equation}
\begin{defi}
Une chaîne de Markov est indépendante dans le temps si $q(x_{n+1}|x_{n})$ ne dépend pas de $n$. 
\end{defi}

Une chaîne de Markov indépendante du temps est caractérisée entièrement par un vecteur décrivant l'état initial $\vec{\nu}_{0}$ et une matrice de transition de probabilité $q = [q_{ij}]\ \forall\ i, j \in \{1,2, ..., m\}$. L'état initial est représenté par un vecteur de dimension $m$ et il contient la distribution sur les états au temps 0. Soit $\nu_{0}$ l'état initial, alors $\vec{\nu}_{1} = q^{T}\vec{\nu}_{0}$. De manière plus générale, 
\begin{equation}
\vec{\nu}_{n} = (q^{T})^{n}\vec{\nu}_{0}.
\end{equation}
Ainsi, la matrice de probabilité de transition peut être vu comme un opérateur d'évolution temporelle sur un état. 

\begin{defi}
Un processus stochastique est appelé stationnaire si 
\begin{equation}
q(X_1 = x_1, X_2 = x_2, ..., X_n = x_n) = q(X_{1+l} = x_1, X_{2+l} = x_2, ..., X_{n+l} = x_n)
\end{equation}
pour tout temps $n$, translation dans le temps $l$ et $x_1, x_2, ..., x_n \in S.$
\end{defi}


\begin{defi}
Une distribution sur les états $\vec{\nu}_n$ est une distribution stationnaire si 
\begin{equation}
\vec{\nu}_{n} = q^{T}\vec{\nu}_{n}.
\end{equation}
\end{defi}

Selon ces définitions, la distribution stationnaire est un vecteur propre de valeur propre $+1$ de la matrice de transition. L'existence et l'unicité de cette distribution stationnaire provient des concepts d'irréductibilité et d'apériodicité d'une chaîne de Markov.

\begin{defi}
Une chaîne de Markov est irréductible s'il est possible d'effectuer une transition de n'importe quel état vers tous les autres états en un temps fini.
\end{defi}

Une chaîne de Markov indépendante du temps peut être représentée par un graphe de transition qui est équivalent à la matrice de transition. Soit une matrice de transition pour une chaîne de Markov à $m$ états, alors le graphe de transition correspondant contiendra $m$ noeuds représentant chacun des états. De plus, pour chaque élément non nul de la matrice $P_{ij}$, une flèche relie le noeud de l'état $i$ au noeud de l'état $j$. La figure \ref{fig:markov} présente un exemple de graphe de transition décrivant la matrice de transition suivante, 
\begin{equation}
q=
  \begin{pmatrix}
    0 & 0 & q_{13} \\
    q_{21} & 0 & q_{23} \\
    0 & q_{32} & q_{33}
  \end{pmatrix}.
\end{equation}
Dans cet exemple, il est possible de parcourir un chemin dans ce graphe pour passer d'un état à l'autre. Le nombre de chemins emprunté donne la valeur du pas de temps nécessaire. Suivant l'exemple de la figure \ref{fig:markov}, si l'état initial est $S_1$, alors le temps minimal requis pour rejoindre l'état $S_2$ est de 2. Par cette méthode de comptage, il est possible de définir la \emph{période} d'un état $S_i$ comme étant le plus grand commun diviseur de l'ensemble des temps requis pour revenir à l'état $S_i$. Si la période est de 1, alors l'état est \emph{apériodique}. Une chaîne de Markov avec tous ses états apériodiques est nommée une chaîne de Markov apériodique. 

\begin{defi}
Soit $\mathcal{P}$ une chaîne de Markov indépendante du temps et à états finis. Si $\mathcal{P}$ est irréductible et apériodique, alors la chaîne de Markov est ergodique.
\end{defi}

\begin{defi}
Soit $\mathcal{P}$ une chaîne de Markov ergodique, alors il existe une unique distribution stationnaire $\vec{\nu}_s$ et pour toute distribution $\vec{\nu}_0$, 
\begin{equation}
\lim_{n \rightarrow \infty} (q^T)^n \vec{\nu}_0 = \vec{\nu}_s.
\end{equation}
\end{defi}

La chaîne de Markov de la figure \ref{burstchannel} est irréductible et apériodique, donc ergodique. Il existe donc un unique état stationnaire qui satisfait $q^T\vec{\nu} = \vec{\nu}$. Avec comme matrice de transition

\begin{equation}
q=
  \begin{pmatrix}
    q(B|B) & q(B|M) \\
    q(M|B) & q(M|M)
  \end{pmatrix}
\end{equation}
 et l'état stationnaire 
\begin{equation} 
 \vec{\nu} = \binom {p_{B}} {p_{M}}.
\end{equation}
 Il suffit donc de résoudre l'équation

\begin{equation}
  \begin{pmatrix}
    q(B|B) & q(B|M) \\
    q(M|B) & q(M|M)
  \end{pmatrix}^{T}
 \binom {p_{B}} {p_{M}} = \binom {p_{B}} {p_{M}}.
\end{equation}
Ce qui donne,
\begin{equation}
\binom{p_{B}} {p_{M}} = \frac{1}{q(M|B) + q(B|M)} \binom{q(B|M)} {q(M|B)}.
\end{equation}

\begin{figure}\centering
\includegraphics[scale=0.8]{\figpath/chaine_markov.pdf}
\caption[Processus de Markov]{Exemple d'un diagramme représentant un processus de Markov à 3 états.\label{fig:markov}}
\end{figure}

%\chapter{Échangeabilité est théorème de Finetti}

%\begin{defi}
%Une séquence de variable aléatoire $Y_1$, $Y_2$, ..., $Y_N$ est échangeable si la probabilité jointe est invariante sous permutation. C'est-à-dire, pour toute permutation $\pi$ on a,
%$$
%P(Y_1, ..., Y_N) = P(Y_{\pi_1}, ..., Y_{\pi_N})
%$$
%\end{defi}

%\chapter{Réseaux de tenseur}

%  \subsection{MPS, MPO et PEPS}
%  Un état quantique général représentant un système en 1 dimension de $N$ particules à $d$ niveaux est généralement représenté par une fonction d'onde nécéssitant $O(d^N)$ éléments. Ceci rend la simulation de tel système inéfficace sur un ordinateur classique. Il est possible d'utiliser la décomposition en valeurs singulières pour écrire la fonction d'onde dans la base de Schmidt. La figure \ref{fig:mps} montre cette situation. Les liens verticaux sont appelés liens physiques et ils représentent le degrée de liberté des particules. Tandis que les liens verticaux sont appelés liens virtuels. Ils encodent l'entropie d'intrication du système. Il s'avère que pour une grande classe d'état quantique, il est possible de tronquer la dimension des liens virtuels tout en gardant une bonne représentation de l'état initial. Ces états sont nommées \emph{états à produit de matrices (MPS; Matrix product states)}. Le principal attrait de ces états provient du fait qu'il est possible d'utiliser simplement une complexité de mémoire de $O(poly(N))$ pour les spécifier. 


%  \begin{figure}\centering
%  \includegraphics[scale=0.7]{\figpath/mps.pdf}
%  \caption[MPS]{Illustration de la décomposition d'un état quantique $\ket \psi$ dans la base de Schmidt par l'exécution successive d'une décomposition en valeurs singulières. Les tenseurs rouges sur l'axe virtuel sont les matrices diagonales contenant les coefficients de Schmidt. Ceux-çi encodent en quelque sorte l'entropie d'intrication du système. Une troncation adéquate des coefficients de Schmidt permet une représentation efficace de plusieurs systèmes quantiques. \note{Trop long...} \label{fig:mps}}
%  \end{figure}

%  \begin{figure}\centering
%  \includegraphics[scale=0.7]{\figpath/mpo.pdf}
%  \caption[MPO]{Exemple d'un \emph{MPO} obtenu par le calcul $\rho = \ket \psi \bra \psi$. Les tenseurs jaunes représentes les conditions frontières. \label{fig:mpo}}
%  \end{figure}

%  On peut généraliser les MPS pour le cas des états mixtes. On parlera alors d'\emph{opérateur à produits de matrice (MPO; Matrix product operator)}. Ils servent entre autre à représenter des Hamiltoniens locaux et des circuits quantiques de faibles profondeurs. La figure \ref{fig:mpo} illustre la forme générale d'un \emph{MPO}. Dans la situation où les conditions frontières ne sont pas périodique on retrouvera des termes frontières qui sont généralement des tenseurs de rang 1. De manière générale, un \emph{MPO} peut s'utiliser dans le cas d'opérateur qui augmente peu l'entropie d'intrication d'un état quantique.

%  Au niveau théorique, on dénote l'application des \emph{MPS} pour la classification des phases quantiques de la matière pour des systèmes en 1D \note{ref}. Du point de vue algorithmique, les \emph{MPS} et \emph{MPO} sont utilisés pour trouver l'état de plus faible énergie de certains systèmes en 1D. Notons que le problème de recherche de l'état de plus faible énergie dérivant de Hamiltonien local est un problème \emph{QMA-complet}, c'est-à-dire que même avec l'aide d'un ordinateur quantique, la solution de ce problème ne se trouve pas efficacement. Ainsi, les \emph{MPS} sont une paramètrisation efficace d'un petit sous-ensemble de tout les états quantiques possibles, or il s'avère quand dans la majorité des cas, ce sous-ensemble contient les états d'intérêts \note{ref}.  

%  Il est aussi possible de généraliser le \emph{MPS} au dimension supérieure, on parle alors de \emph{paire d'états intriqués projetée (PEPS; projected entangled pair states)}. Dans le cas de système en 2 dimensions, on obtient un réseau de tenseurs illustré à la figure \note{image}. Ce type d'ansatz peut servir à décrire par exemple le code de Kitaev qui est un code de correction d'erreurs quantiques basé sur un modèle ayant un ordre topologique \note{citation}.

%  \subsection{MERA et bMERA}
%  \note{Expliquer concept de cone causal}

\chapter{Calcul de la capacité du canal de Gilbert-Elliott}

Le calcul de la capacité du modèle de Gilbert-Elliott peut être effectué grâce à la formule \ref{capagilbert}. On propose d'utiliser les réseaux de tenseurs pour effectuer le calcul de la capacité. À titre d'exemple, les figures présentées sont pour $n = 3$, mais on peut facilement généraliser pour $n$ plus grand. 

Il faut donc calculer le terme $Q_n$, observons la relation suivante :
\begin{equation}
Q_n = P(z_n = 1| z_1^{n-1},s_0) = \frac{P(z_n = 1,z_1^{n-1}|s_0)}{P(z_1^{n-1}|s_0)}
\end{equation}

En termes de réseaux de tenseurs,
\begin{equation}
\includegraphics[scale=0.4]{\figpath/qthree.pdf}
\end{equation}

La valeur moyenne de l'entropie binaire de $Q_3$ est donc donnée par,

\begin{equation}
\includegraphics[scale=0.4]{\figpath/eqthree.pdf}
\end{equation}

Pour les capacités de la table \ref{noise_param}, la valeur $n = 12$ a été utilisé.