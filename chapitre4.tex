% !TEX root = memoire.tex

\chapter{Méthodologie} \label{metho}

Dans ce chapitre, la méthodologie utilisée pour la réalisation du projet de recherche est introduite. Il s'agit d'appliquer les réseaux de tenseurs vus précédemment dans un contexte autre que celui de la mécanique quantique, soit en correction d'erreurs classique. Plus précisément, le but du projet de recherche est l'implémentation d'un décodeur efficace pour les codes polaires et codes polaires convolutifs soumis au bruit avec mémoire. Premièrement, une correspondance entre le problème du décodage et celui de la contraction d'un réseau de tenseurs est exposée. Ensuite, une formulation du décodeur par annulation successive en termes d'un algorithme faisant intervenir un réseau de tenseurs est donnée. Finalement, une description du modèle de bruit à états finis en termes d'un réseau de tenseurs est démontrée.

\section{Formulation d'un circuit en termes des réseaux de tenseurs}
		%\note{Montrer qu'une porte logique peut s'écrire comme un tenseurs}

		%En informatique classique et quantique, une bonne manière d'illustrer les algortihmes est par l'utilisation d'un circuit logique. De même, le circuit d'encodage de plusieurs codes linéaires peuvent être exprimés sous forme d'un circuit. Typiquement, un circuit contient un nombre $n$ de lignes parallèles représentant chacun des bits (qubits) auxquelles on ajoute des portes logiques qui peuvent agir sur plusieurs bits. La figure \ref{fig:circuit} montre un exemple de circuit à 5 bits. Les portes bleus, rouges et vertes sont respectivement des portes à 1, 2 et 3 bits.

		%\begin{figure}\centering
		%\includegraphics[scale=0.8]{\figpath/circuit.pdf}
		%\caption[Circuit]{Circuit à 5 bits \label{fig:circuit}}
		%\end{figure}

		%On peut représenter un circuit à $N$ bits par une matrice dans la base des chaînes de $N$ bits. Généralement, cette matrice augmente de manière exponentielle avec le nombre de bits du circuit. Imaginons que nous souhaitons faire agir un circuit à $n$ bits sur un état d'entrée initial. Cet état est représenté par un vecteur de dimension $2^n$. L'obtention de l'état à la sortie s'effectue donc par un produit matrice vecteur, comme la matrice est de dimension $2^n \times 2^n$ le coût en calculs sera de $O(2^{3n})$, par ailleurs le coût en mémoire sera de $O(2^{2n})$. Il n'est donc pas très efficace d'utiliser cette représentation matricielle pour un circuit. Clairement, la structure du circuit peut être utilisé pour un calcul plus efficace. En effet, les réseaux de tenseurs peuvent être appliqué pour représenter efficacement un circuit. Pour ce faire, il suffit d'assigner un tenseur pour chaque porte logique. Par exemple, une porte à deux bits tel que le CNOT peut être formulé comme un tenseur de rang 4 définit par 4 indices : $N_{ijkl}$. Les bits peuvent être vu comme des tenseurs de rang 1 (vecteur) avec la convention suivante : $$0 = \binom 10 \quad \quad 1 = \binom 01.$$

Un circuit d'encodage d'un code de correction d'erreurs peut se décrire comme un réseau de tenseurs. Pour ce faire, il suffit d'assigner les états de bits 0 et 1 à des tenseurs de rang 1
\begin{equation}
\includegraphics[scale=0.7]{\figpath/0.pdf} \qquad \qquad
\includegraphics[scale=0.7]{\figpath/1.pdf}.
\end{equation}
De cette manière, une porte logique représente un tenseur. Dans ce cas, la porte non contrôlé est un tenseur de rang 4 avec chacun des 4 indices représentant les bits d'entrée et de sortie. Ainsi, ce tenseur prend la valeur 1 lorsque les assignations d'indices correspondent aux assignations données par la table de vérité \ref{vérité}, autrement le tenseur vaut 0. Par exemple, 
$$
\includegraphics[scale=0.7]{\figpath/bon_cnot.pdf} \qquad \qquad
\includegraphics[scale=0.7]{\figpath/mauvais_cnot.pdf}.
$$ 
Ainsi, un ensemble de portes logiques représentant un circuit est décrit en termes d'un réseau de tenseurs. Par exemple, le circuit d'encodage d'un code polaire convolutif à $N = 4$ bits est représenté par un réseau de tenseurs contenant 5 tenseurs de rang 4. La contraction de ce réseau de tenseurs avec une chaîne de bits en entrée permet d'obtenir une chaîne à la sortie.
Par exemple, 
$$
\includegraphics[scale=0.7]{\figpath/encodage.pdf}.
$$
De manière générale, le circuit d'encodage est illustré comme un tenseur de rang $2N$ avec $N-k$ positions où les états de bits d'entrée sont gelés à la valeur 0, il s'agit de l'ajout de redondance au message. Par exemple, 
$$
\includegraphics[scale=0.7]{\figpath/encodage_tn.pdf}.
$$

%===============================================================================
\section{Le décodage, un problème de contraction}

L'application des réseaux de tenseurs dans le contexte de la théorie des codes s'établit grâce à une correspondance entre le problème de contracter un réseau de tenseurs et le problème de décodage d'un code de correction d'erreurs. Cette correspondance permet l'implémentation du décodeur par annulation successive en termes de réseaux de tenseurs. Avant de développer cette correspondance, il est important de comprendre comment représenter et manipuler une densité de probabilité dans le langage des réseaux de tenseurs. 

\subsection{Calculs sur une densité de probabilité}
Soit une densité de probabilité sur $k$ bits $P(\vec{u})$, où $\vec{u}$ peut être interprété comme la chaîne de bits de message dans le contexte de la correction d'erreurs. Cet objet peut être décrit comme un vecteur contenant $2^k$ éléments. Celui-ci peut facilement s'écrire comme un tenseur de rang $k$ avec chaque indice de dimension 2 représentant chaque bit obtenu en appliquant un remodelage par extension d'indices. Cette densité de probabilité peut donc se représenter comme 
\begin{equation}
 	%\begin{figure}\centering
	\includegraphics[scale=0.6]{\figpath/density.pdf}.
	%\caption[Densité de probabilité]{Représentation d'une densité de probabilité sur les chaînes de $N$ bits par un tenseur de rang $N$ \note{Mettre dans une vraie équation ?}. \label{fig:density}}
	%\end{figure}
\end{equation}
Pour trouver la chaîne de bits la plus probable $\vec{u}$, il faut utiliser le décodage par maximum de vraisemblance. Pour ce faire, il suffit de trouver l'argument qui maximise cette probabilité. Il s'agit donc de calculer 
\begin{equation}
\includegraphics[scale=0.6]{\figpath/proba.pdf}.
\end{equation}
Ce problème d'optimisation est intraitable, c'est-à-dire qu'il faut un nombre de calculs exponentiel pour obtenir le résultat. En effet, il s'agit du même problème que celui du décodage optimal de l'équation \ref{decodeuroptimal}. Ainsi, pour des raisons d'efficacité, au lieu d'optimiser en fonction de la chaîne la plus probable, une technique d'optimisation itérative sur chacun des bits est utilisée. Cette technique est à la base du décodeur par annulation successive. Pour ce faire, il est important de montrer comment effectuer les calculs de probabilité conditionnelle et marginale sur la densité de probabilité. Pour calculer la probabilité conditionnelle $P(u_1, u_2, u_4| u_3 = 1, u_5 = 0, u_6 = 1)$ à une constante de normalisation près, il faut effectuer la contraction,
\begin{equation}
	%\begin{figure}\centering
	\includegraphics[scale=0.6]{figs/conditional.pdf}.
	%\caption[Densité de probabilité]{Calcul d'une probabilité conditionel \note{Mettre dans une vraie équation ?}. \label{fig:cond}}
	%\end{figure}
\end{equation}
Pour le calcul d'une probabilité marginale 
\begin{equation}
P(u_2, u_3, u_4) = \sum_{u_1,u_5,u_6} P(\vec{u}),
\end{equation}
il s'agit de sommer sur les indices. Pour ce faire, le tenseur de rang 1 suivant est introduit,
\begin{equation}
\includegraphics[scale=0.7]{\figpath/e.pdf}.
\end{equation}
Cette probabilité marginale peut donc se calculer par l'équation suivante,
\begin{equation}
	%\begin{figure}\centering
	\includegraphics[scale=0.6]{figs/marginal.pdf}.
	%\caption[Densité de probabilité]{Calcul d'une probabilité marginale \note{Mettre dans une vraie équation ?}. \label{fig:marge}}
	%\end{figure}
\end{equation}

\subsection{Décodeur par annulation successive}
Il est donc possible de faire correspondre des calculs de probabilités marginales et conditionnelles à un problème de contraction d'un réseau de tenseurs. Le principe de base du décodeur par annulation successive s'illustre facilement en utilisant les réseaux de tenseurs. Tel qu'expliqué à la section \ref{decodeurannulationsuccessive}, ce décodeur procède en décodant un seul bit à la fois de manière itérative en allant de droite à gauche. Supposons une densité de probabilité $P(\vec{u})$ sur $N$ bits. Pour maximiser chaque bit de droite à gauche du circuit, il faut calculer $\hat{u}_i = \arg\max_{u_i \in \{0,1\}}P(u_i|u_1, u_2, ..., u_{i-1})$ pour chacune des positions. Ainsi, en termes de réseaux de tenseurs cette équation s'écrit comme 
\begin{equation}
	\includegraphics[scale=0.6]{figs/scd_tn.pdf}.
\end{equation}
Cette manière de procéder est sous-optimale puisqu'elle n'optimise pas en fonction de la chaîne de bits globale, mais elle est beaucoup plus efficace puisqu'il n'y a que 2 valeurs de bits possibles. C'est sur ce principe qu'est basé le décodeur par annulation successive. Par contre, la contraction de ce tenseur n'est en général pas efficace. Dans le cas des codes polaires et des codes polaires convolutifs, il est possible de contracter ce réseau de tenseurs efficacement, les explications sont données à la section \ref{decodage}. Pour obtenir la chaîne totale, il faut donc effectuer $N$ itérations de ce type. Jusqu'ici, l'obtention de la densité de probabilité sur les chaînes de bits a été supposée. Pour la suite, il est question de comment l'obtenir dans le contexte du décodage des codes polaires et des codes polaires convolutifs.

\subsection{L'obtention de la densité de probabilité}
Soit un circuit d'encodage $\mathcal{C}$ pour un code polaire ou un code polaire convolutif à $N = 2^n$ bits. Tel que vu précédemment, ce circuit contient des portes logiques et celles-ci peuvent être vues comme des tenseurs. Le circuit ainsi obtenu est donc un tenseur de rang $2N$ avec $N$ indices pour les bits d'entrées du circuit et $N$ indices pour les bits de sortie du circuit. Supposons un modèle de bruit binaire générique $\mathcal{W}$ agissant sur les $N$ bits\footnote{Le modèle de bruit est traité plus en détail à la section suivante.}. Ce modèle de bruit peut aussi se représenter par un tenseur de rang $2N$. Un remodelage de ce tenseur en une matrice $2^N$ par $2^N$ donne une matrice stochastique telle que la somme de chacune de ses colonnes donne 1. Puis, l'ensemble des bits reçus à la sortie du canal se représente par un ensemble de tenseurs de rang 1 $y_1, y_2, ..., y_N$. La densité de probabilité sur les chaînes de bits d'entrée possibles $P$ est donc donnée par
\begin{equation}
	\includegraphics[scale=0.6]{\figpath/enc_noise.pdf}.
\end{equation}
Cette formulation est très générale et elle peut s'appliquer pour plusieurs circuits d'encodage $\mathcal{C}$ et modèle de bruit $\mathcal{W}$. Ainsi, la contraction de ce réseau de tenseurs conditionné sur les bits de redondance spécifiés au décodeur donne une densité de probabilité binaire sur les messages envoyés. Le décodeur à maximum de vraisemblance recherchera donc la chaîne de bit la plus probable. Bien entendu, ce cas générique est intraitable, mais il est possible de spécifier $\mathcal{C}$ et $\mathcal{W}$ pour lesquels ce schéma peut s'appliquer de manière efficace lorsqu'un décodeur par annulation successive est utilisé.

\section{Modèle de réseaux de tenseurs pour canal bruyant}
Dans cette section, il est présenté une formulation en termes de réseaux de tenseurs pour décrire les canaux de type binaire symétrique sans mémoire et les canaux avec mémoire à états finis.

\subsection{Canaux sans mémoire}

Tel que vu précédemment, un modèle de bruit binaire symétrique est décrit grâce à $W(Y|X)$, une matrice contenant les probabilités conditionnelles. Ainsi, formulé en termes de réseaux de tenseurs, il suffit de représenter ce type de canal par un tenseur de rang 2 avec des indices représentant l'entrée $X$ et la sortie $Y$
\begin{equation}
W(Y|X) = 
	\begin{gathered}
	\includegraphics[scale=0.6]{\figpath/canal_binaire.pdf}.
	\end{gathered}
\end{equation}
De cette manière, les tenseurs représentant les états de bits peuvent être utiliser pour calculer la probabilité conditionnelle. Par exemple, pour un canal binaire symétrique de probabilité d'erreur $p$, 
\begin{equation}
W(Y = 1|X = 0) = 
	\begin{gathered}
	\includegraphics[scale=0.6]{\figpath/canal_binaire_ex.pdf}
	\end{gathered}
= p.
\end{equation}

Pour une séquence de $N$ bits $\vec{x}$ soumise à $N$ copies indépendantes de ce canal, la probabilité d'obtenir la chaîne $\vec{x}$ à la sortie du canal sachant l'entrée est donnée par $W(\vec{y}|\vec{x}) = \prod_{k=1}^N {W(y_k|x_k)}$, une caractéristique fondamentale d'un modèle de bruit sans mémoire. En termes de réseaux de tenseurs, 
\begin{equation}
W(\vec{y}|\vec{x}) = \prod_{k=1}^N {W(y_k|x_k)} =
	\begin{gathered}
	\includegraphics[scale=0.6]{\figpath/canal_iid.pdf}.
	\end{gathered}
\end{equation}

\subsection{Canaux avec mémoire}

Considérons le cas du canal avec mémoire à états finis avec $d$ états en supposant que la dynamique répond à un modèle stochastique correspondant à une chaîne de Markov ergodique. Tel que vu à la section \ref{bruitmemoire}, un canal de Markov à états finis est décrit par l'équation, 
\begin{equation}
P_{N}(\vec{y}|\vec{x}, s_0) = \sum_{s_1^N}\prod_{k=1}^Nq(s_k|s_{k-1})p(y_k|x_k,s_{k-1}).
\end{equation}
De manière équivalente,
\begin{equation}
P_{N}(\vec{y}|\vec{x}, s_0) = \sum_{s_1^N}\prod_{k=1}^N\sum_{c}q(s_k|c)\delta_{c,s_{k-1}}p(y_k|x_k,c).
\end{equation}
Il est possible d'associer les tenseurs suivants à l'équation précédente,
\begin{equation}
\includegraphics[scale=0.5]{\figpath/fsc_tn.pdf}.
\end{equation}
Ainsi, en termes de réseaux de tenseurs un canal de Markov à états finis est décrit par le réseau de tenseurs suivant,
\begin{equation}
\includegraphics[scale=0.5]{\figpath/fsc_markov_tn.pdf}.
\end{equation}
Ce réseau de tenseurs possède une topologie en chaîne bien connue dans le domaine de la physique des systèmes quantiques à $N$-corps, il porte le nom d'opérateur à produit de matrices\footnote{Dans la littérature, ce terme fait référence à \emph{matrix product operator (MPO)}.} \cite{mpo}. De manière plus intuitive, ce type de canal est décrit par une chaîne combinant un tenseur de rang 2 décrivant la matrice de transition des états et un tenseur de rang 4 décrivant l'espace des canaux $W_{C}(Y|X)$. De plus, comme l'effet de ce bruit est étudié sur des chaînes de bits finies, deux tenseurs de rang 1 agissant comme conditions frontières sont définis. Le tenseur de rang 1 sur la frontière de droite représente la distribution initiale $S_{0}$ comme étant la distribution stationnaire décrite par le vecteur $\vec{\nu}_{s}$. Puis, le tenseur de rang 1 sur la frontière de gauche représente une somme sur tous les états finaux $S_N$, encodant le fait qu'à la fin du processus l'état du canal $C$ peut prendre n'importe quelle valeur. Cette somme est réalisée par un vecteur dont l'ensemble des composantes est 1. Ces deux conditions frontières sont représentées par,
\begin{equation}
\includegraphics[scale=0.5]{\figpath/right_state.pdf},
\end{equation}
\begin{equation}
\includegraphics[scale=0.5]{\figpath/left_state.pdf}.
\end{equation}
Dans le cas du modèle de Gilbert-Elliott, les tenseurs sont définis comme
\begin{equation}
\includegraphics[scale=0.6]{\figpath/corr.pdf},\
\end{equation}
\begin{equation}
\includegraphics[scale=0.6]{\figpath/canal_2_etats.pdf}\ .
\end{equation}

%Soit, $s_{0}$ comme étant la distribution stationnaire, il s'agit d'un tenseur de rang 1 représenté par le vecteur $\vec{\nu}_{s}$. Puis, $s_{N}$ représenté par le vecteur contenant 1 à toutes les positions encodant le fait qu'à la fin du processus l'état du canal $C$ peut prendre n'importe quelle valeur.

\section{Algorithme de décodage}\label{decodage}


		\begin{figure}\centering
		\includegraphics[scale=0.7]{figs/conv_pc.pdf}
		\caption[Réseau de tenseurs utile pour le décodage du bit $u_5$]{Réseau de tenseur illustrant une étape du décodeur par annulation successive. Les bits $u_1$ à $u_4$ sont supposés connus du décodeur. La contraction de ce tenseur donne un vecteur contenant les probabilités des valeurs pour le bit $u_5$.\label{fig:dec16bits}}
		\end{figure}

		\begin{figure}\centering
		\includegraphics[scale=0.7]{figs/conv_pc_simplification.pdf}
		\caption[Simplification du réseau de tenseurs]{Illustration de l'étape de simplification du réseau de tenseurs utilisé pour le décodage du bit $u_5$.\label{fig:cpc8bits_simp}}
		\end{figure}		


		\begin{figure}\centering
		\includegraphics[scale=0.7]{figs/arbre_tn.pdf}
		\caption[Structure d'arbre avec des feuilles connectées]{Après simplification, le réseau de tenseurs obtenu correspond à un circuit ayant la topologie d'un arbre dont les feuilles sont attachées horizontalement par une chaîne due au modèle de bruit avec mémoire. \label{fig:arbre_tn}}
		\end{figure}	


Les sous-sections précédentes montrent qu'il est possible d'utiliser les réseaux de tenseurs pour décrire le circuit d'encodage et le modèle de bruit. En principe, les réseaux de tenseurs peuvent être utilisés pour effectuer un décodage selon l'algorithme par annulation successive pour un code polaire ou un code polaire convolutif soumis au bruit avec mémoire. Dans cette sous-section, il est question d'appliquer ces outils de manière efficace. Pour le décodage d'un code de la famille des codes polaires\footnote{Le terme famille des codes polaires fait référence aux généralisations possibles telles que celle du code polaire convolutif.}, un ensemble de bits gelés $\mathcal{F}$ est supposé connu, ces bits sont fixés à la valeur 0 par convention. En utilisant la théorie développée à la section 4.2, on note qu'il suffit de spécifier un circuit $\mathcal{C}$ et un modèle de bruit $\mathcal{W}$. La figure \ref{fig:dec16bits} présente un exemple du décodage du bit $u_5$ par l'algorithme d'annulation successive pour un code polaire (portes logiques noires) et un code polaire convolutif (portes logiques noires et bleues) de taille $N = 8$ bits dans un contexte de bruit avec mémoire. A priori, la contraction du réseau de la figure \ref{fig:dec16bits} semble difficile, mais il est possible d'utiliser des identités algébriques simplifiant grandement le circuit. Il s'agit des simplifications suivantes,
\begin{equation}
\includegraphics[scale=0.7]{\figpath/0_simplification.pdf}\;, \quad
\includegraphics[scale=0.7]{\figpath/1_simplification.pdf}\;, \quad
\includegraphics[scale=0.7]{\figpath/e_simplification.pdf}.
\end{equation}
Ainsi, l'utilisation de ces simplifications couplées au décodeur par annulation successive permet une contraction efficace du réseau de tenseurs et donc un décodage efficace. Typiquement, l'algorithme de décodage pour un bit donné s'effectue en 2 étapes, soit une étape de simplification du circuit grâce à l'utilisation des identités, suivie d'une étape de contraction des tenseurs du bas vers le haut. La figure \ref{fig:cpc8bits_simp} montre un exemple de simplification du circuit pour $N = 8$ bits. Cette simplification permet d'obtenir un circuit ayant la même topologie qu'un réseau de tenseurs en arbre dont les feuilles sont connectées à un réseau de tenseurs ayant la topologie d'une chaîne correspondant au bruit avec mémoire comme le montre la figure \ref{fig:arbre_tn}. Une fois les simplifications complétées, il ne reste plus qu'à contracter ce réseau de tenseurs. Un réseau de tenseur ayant la topologie d'un arbre de profondeur $\log{N}$ peut se contracter efficacement en contractant du bas vers le haut résultant en une complexité de calculs de $O(N)$. Dans le cas du réseau de la figure \ref{fig:arbre_tn}, il ne s'agit pas complètement d'un arbre puisque les feuilles sont connectées horizontalement. Par contre, on peut adapter la contraction du bas vers le haut d'un arbre en effectuant une contraction intermédiaire entre chacune des paires de feuilles connectées au bas du circuit. Il s'agit spécifiquement d'effectuer cette contraction 
\begin{equation}
\includegraphics[scale=0.7]{\figpath/contract_noise.pdf}.
\end{equation}
Comme la dimension de l'indice horizontal est $d$, représentant le nombre d'états du canal avec mémoire, on trouve que le coût de cette contraction est $O(d^3)$. Ainsi, partant du circuit de la figure \ref{fig:arbre_tn}, la série de contraction suivante est effectuée:
$$
\includegraphics[scale=0.7]{figs/contract_1.pdf}
$$
$$
\includegraphics[scale=0.7]{figs/contract_2.pdf}
$$
$$
\includegraphics[scale=0.7]{figs/contract_3.pdf}
$$
$$
\includegraphics[scale=0.7]{figs/contract_4.pdf}
$$
$$
\includegraphics[scale=0.7]{figs/contract_5.pdf}.
$$

%illustrée aux figures \ref{fig:contract_2} à \ref{fig:contract_5}. 
Finalement, il suffit de calculer 
$$
\includegraphics[scale=0.7]{\figpath/contract_6.pdf}.
$$
De manière générale, le coût de contraction pour un réseau de tenseurs ayant un circuit en arbre dont les feuilles sont attachées à un modèle de bruit avec mémoire ayant la topologie d'une chaîne est de $O(d^3 N)$. Dans l'exemple donné ici, il s'agit seulement du décodage d'un seul bit. Il faudrait donc itérer la procédure de décodage pour les $N$ positions du circuit, en effectuant une procédure de simplification et de contraction à chaque fois. Lors d'une implémentation du décodeur, il est possible de recycler plusieurs étapes de calculs reliées à la simplification et à la contraction du circuit de sorte qu'au total l'algorithme nécessite une complexité de calculs de $O(d^3 N \log{N})$.

%On traitera de l'algorithme de sélection des bits gelés à la prochaine sous-section. Si on dénote $U_1^N$ l'ensemble des bits à encoder alors $U^{\mathcal{F}}$ dénote les bits gelés et $U^{\mathcal{F}^c}$ l'ensemble des bits du message. L'encodage s'effectue par un circuit qui peut être vu comme une matrice réversible $G$ donnant ainsi un mot code $X_1^N = G U_1^N$ \note{Vérifier convention}. Puis, le canal produit une nouvelle chaîne de bits $Y_1^N$. Ainsi, le but du décodeur par annulation successive est d'optimiser la fonction de vraisemblance $L(U_1^i|Y_1^N) = \sum_{U_{i+1}^N}W_{N}(Y_1^N|GU_1^N)$ \note{$L(U_i|Y_1^N, U_1^{i-1}) = \sum_{U_{i+1}^N}W_{N}(Y_1^N|GU_1^N)$} pour chaque $i \in \mathcal{F}^{c}$. 

%La figure \note{faire figure} en montre un exemple. Dans le cas des codes polaires, le circuit initial comporte $\frac N 2 \log N$ portes et on peut voir que pour chaque itération du décodage, on conserve simplement $2^{l-1}$ portes par couches si on dénote $l = 1$ pour la première couche. Ainsi, pour un code polaire à $n$ couches on obtient une structure contenant $\sum_{l = 1}^{n}2^{l-1} = 2^{n}-1 = N-1$ portes. La figure ref montre une étape du décodage avec le cone causal résultant. L'obtention du cone causal s'obtient grâce à un algorithme de propagation des états de bits (tenseurs de rang 1) à travers le circuit en utilisant les identités. On procède généralement couche par couche en commençant par la couche du haut. Cette étape peut être réalisée en parallèle. 

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/conv_pc.pdf}
		%\caption[Réseaux de tenseurs utile pour le décodage du bit $u_5$]{Réseau de tenseur illustrant une étape du décodeur par annulation successive. Les bits $u_1$ à $u_4$ sont supposés connus du décodeur. La contraction de ce tenseur donne un vecteur contenant les probabilités des valeurs pour le bit $u_5$.\{fig:dec16bits}}
		%\end{figure}

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/conv_pc_simplification.pdf}
		%\caption[Simplification]{ \label{fig:cpc8bits_simp}}
		%\end{figure}		


		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/arbre_tn.pdf}
		%\caption[Arbre]{ \label{fig:arbre_tn}}
		%\end{figure}	

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/contract_1.pdf}
		%\caption[Contraction 1]{ \label{fig:contract_1}}
		%\end{figure}	


		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/contract_2.pdf}
		%\caption[Contraction 2]{ \label{fig:contract_2}}
		%\end{figure}	

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/contract_3.pdf}
		%\caption[Contraction 3]{ \label{fig:contract_3}}
		%\end{figure}	

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/contract_4.pdf}
		%\caption[Contraction 4]{ \label{fig:contract_4}}
		%\end{figure}	

		%\begin{figure}\centering
		%\includegraphics[scale=0.7]{figs/contract_5.pdf}
		%\caption[Contraction 5]{ \label{fig:contract_5}}
		%\end{figure}	


		%\begin{figure}\centering
		%\includegraphics[scale=0.4]{figs/identities.pdf}
		%\caption[Décodage]{ \label{fig:identités}}
		%\end{figure}
%\note{Faire Pseudo-Code}

%\note{Montrer les cas de contraction possible}

\section{Sélection des bits gelés}

Une des étapes cruciales pour la construction de bons codes dans la famille des codes polaires est la sélection des bits gelés. Cette sélection est déterminante pour obtenir un code atteignant la capacité. Intuitivement, la position des bits gelés devrait être aux endroits où les canaux synthétiques obtenus par le phénomène de polarisation sont le plus médiocres. Ainsi, cette sélection dépend du modèle de bruit. Dans l'article original d'Arikan, une sélection des bits gelés basée sur le paramètre de Battacharyya d'un canal $Z(W)$ est utilisée. Il s'agit d'une quantité qui mesure la distance entre deux densités de probabilité reliées à l'entrée et la sortie du canal. Il s'avère que pour le canal à effacement, cette quantité se calcul exactement. Par contre, pour les autres types de canaux, tel que le canal binaire symétrique, le calcul de $Z(W)$ est intraitable. Les réseaux de tenseurs offrent une méthode alternative pour déterminer la position des bits gelés. L'algorithme consiste à évaluer la probabilité $E(i)$ d'obtenir une erreur indétectable en position $i$ sachant qu'aux positions 1 à $i-1$ aucune erreur n'a eu lieu. Cette probabilité est évaluée pour chacune des positions de 1 à $N$. Ensuite, ce vecteur de probabilité est classifié de manière à geler $N(1-R)$ bits aux positions qui ont les probabilités d'une erreur indétectable les plus élevées.  

De par son interprétation en termes de réseaux de tenseurs, il est facile d'appliquer cet algorithme autant pour le cas du bruit sans mémoire qu'avec mémoire. Il faut seulement s'assurer que le réseau résultant est efficacement contractable. Pour ce faire, il suffit de poser l'ensemble des bits reçus à 0 puis on applique la même méthode que pour le décodage par annulation successive, donc un décodage de droite à gauche. Cette fois-ci par contre, on emmagasine en mémoire la probabilité $p_1$, correspondant à la probabilité d'une erreur non détectée, pour chaque bit décodé et on pose le bit décodé à la valeur 0. L'équation suivante présente un exemple pour le calcul de $E(N-2)$
\begin{equation}
\includegraphics[scale=0.7]{\figpath/algo_gele.pdf}\ .
\label{eq:algo_gele}
\end{equation}
La complexité de cet algorithme est similaire à celle du décodage, soit de $O(N\log{N})$. Ainsi, avant d'effectuer les simulations numériques pour l'évaluation de la performance d'un code on utilise cette sous-routine pour trouver $\mathcal{F}$.

%\begin{algorithm}[H] %or another one check
% \caption{Algorithme pour la sélection des bits gelés}
%    \SetAlgoLined
%	\KwData{$N$, $n_f$, $\mathcal{C}$, $\mathcal{E}$}
%	\KwResult{$\mathcal{F}$} 
%	$E \longleftarrow$ \emph{zéros}($N$)\;

%	\For{i dans 1 : N}{
%		$E[i] = contraction(i, \mathcal{C}, \mathcal{E})$\;
%	}
%	$\mathcal{F} = sortperm(E)[N:-1:N-n_f+1]$\;
%	\Return{$\mathcal{F}$}
%\label{algo_bit_gelé}
%\end{algorithm}


%\note{Mettre des références}
