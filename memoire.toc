\babel@toc {french}{}
\contentsline {chapter}{Sommaire}{ii}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Th\IeC {\'e}orie et mise en contexte}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Th\IeC {\'e}orie des codes}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Les codes lin\IeC {\'e}aires}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Un exemple: Le code de Hamming}{10}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Th\IeC {\'e}orie de l'information}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}L'entropie au sens de Shannon}{13}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Canal bruyant}{16}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Second th\IeC {\'e}or\IeC {\`e}me de Shannon}{18}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Canal avec m\IeC {\'e}moire et technique de codage}{20}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Canal avec m\IeC {\'e}moire \IeC {\`a} \IeC {\'e}tats finis}{20}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Capacit\IeC {\'e} du canal avec m\IeC {\'e}moire \IeC {\`a} \IeC {\'e}tats fini}{22}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Mod\IeC {\`e}le de Gilbert-Elliott}{23}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Technique d'entrelacement}{25}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Les codes polaires}{27}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}L'id\IeC {\'e}e de base}{27}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Le cas simple du canal \IeC {\`a} effacement}{30}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Th\IeC {\'e}or\IeC {\`e}me de polarisation}{33}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Circuit d'encodage}{34}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}D\IeC {\'e}codeur par annulation successive}{35}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Polarisation du bruit avec m\IeC {\'e}moire}{36}{subsection.2.4.6}
\contentsline {section}{\numberline {2.5}Les r\IeC {\'e}seaux de tenseurs}{37}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}D\IeC {\'e}finition}{38}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Remodelage}{38}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Permutation des indices}{39}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Contraction d'un r\IeC {\'e}seau de tenseurs}{40}{subsection.2.5.4}
\contentsline {subsection}{\numberline {2.5.5}L'ordre de contraction}{41}{subsection.2.5.5}
\contentsline {section}{\numberline {2.6}Les codes polaires convolutifs}{43}{section.2.6}
\contentsline {chapter}{\numberline {3}M\IeC {\'e}thodologie}{46}{chapter.3}
\contentsline {section}{\numberline {3.1}Formulation d'un circuit en termes des r\IeC {\'e}seaux de tenseurs}{46}{section.3.1}
\contentsline {section}{\numberline {3.2}Le d\IeC {\'e}codage, un probl\IeC {\`e}me de contraction}{48}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Calculs sur une densit\IeC {\'e} de probabilit\IeC {\'e}}{48}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}D\IeC {\'e}codeur par annulation successive}{49}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}L'obtention de la densit\IeC {\'e} de probabilit\IeC {\'e}}{50}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Mod\IeC {\`e}le de r\IeC {\'e}seaux de tenseurs pour canal bruyant}{51}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Canaux sans m\IeC {\'e}moire}{51}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Canaux avec m\IeC {\'e}moire}{52}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Algorithme de d\IeC {\'e}codage}{54}{section.3.4}
\contentsline {section}{\numberline {3.5}S\IeC {\'e}lection des bits gel\IeC {\'e}s}{60}{section.3.5}
\contentsline {chapter}{\numberline {4}R\IeC {\'e}sultats et analyse}{62}{chapter.4}
\contentsline {section}{\numberline {4.1}Simulations}{62}{section.4.1}
\contentsline {section}{\numberline {4.2}R\IeC {\'e}sultats}{63}{section.4.2}
\contentsline {section}{\numberline {4.3}Analyse}{64}{section.4.3}
\contentsline {chapter}{Conclusion}{66}{section.4.3}
\contentsline {chapter}{\numberline {A}Cha\IeC {\^\i }nes de Markov}{68}{appendix.A}
\contentsline {chapter}{\numberline {B}Calcul de la capacit\IeC {\'e} du canal de Gilbert-Elliott}{72}{appendix.B}
\contentsline {chapter}{Bibliographie}{73}{equation.B.0.3}
